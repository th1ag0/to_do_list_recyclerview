package com.example.listadetarefas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.listadetarefas.db.AppDatabase
import com.example.listadetarefas.db.dao.TaskDao

import com.example.listadetarefas.ui.TaskAdapter
import com.example.listadetarefas.ui.TaskAdapterListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), TaskAdapterListener {
    lateinit var taskDao: TaskDao
    lateinit var adapter: TaskAdapter

    override fun updateTask(task: Task) {
        taskDao.update(task)
    }

    override fun deleteTask(task: Task) {
        taskDao.delete(task)
    }

    override fun saveTask(task: Task, position: Int) {
        val id = taskDao.insert(task)
        val task = taskDao.findById(id.toInt())
        adapter.saveTask(task!!, position)
        listaTarefas.scrollToPosition(position)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val db =
            Room.databaseBuilder(
                applicationContext,
                AppDatabase::class.java,
                "tarefa.db"
            )
                .allowMainThreadQueries()
                .build()
        taskDao = db.taskDao()

        btAdd.setOnClickListener { addTaskCard() }
        LoadTask()
    }

    private fun addTaskCard() {
        val pos = adapter.addCard( Task("","",""))
        listaTarefas.scrollToPosition(pos)
    }

    private fun LoadTask() {
        val task = taskDao.getAll()
        adapter = TaskAdapter(task.toMutableList(),this)
        listaTarefas.adapter = adapter
        listaTarefas.layoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL, false
        )
    }
}