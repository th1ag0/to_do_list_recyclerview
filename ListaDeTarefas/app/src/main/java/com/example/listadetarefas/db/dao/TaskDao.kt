package com.example.listadetarefas.db.dao

import androidx.room.*
import com.example.listadetarefas.Task

@Dao
interface TaskDao {
    @Query("SELECT * FROM tarefas ORDER BY id DESC")
    fun getAll(): List<Task>

    @Insert
    fun insert(tarefa: Task):Long

    @Update
    fun update(tarefa: Task)

    @Delete
    fun delete(tarefa: Task)

    @Query("SELECT * FROM tarefas WHERE id = :id LIMIT 1")
    fun findById(id:Int):Task?
}