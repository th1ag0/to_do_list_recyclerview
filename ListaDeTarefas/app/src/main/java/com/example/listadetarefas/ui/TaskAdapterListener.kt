package com.example.listadetarefas.ui

import com.example.listadetarefas.Task

interface TaskAdapterListener {

    fun saveTask(task: Task, position: Int)
    fun deleteTask(task: Task)
    fun updateTask(task: Task)
}